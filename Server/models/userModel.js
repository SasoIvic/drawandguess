const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const Schema  = mongoose.Schema;

const userSchema = new Schema({
    'email': {
        type: String,
        required: true,
    },
    'username': {
        type: String,
        required: true,
    },
    'password':{
        type: String, 
        required: true,
    },
    'points': {
        type: Number,
        required: true,
    },
    'gamesPlayedNum': {
        type: Number,
        required: true,
    },
    'winsNum': {
        type: Number,
        required: true,
    },
});

//authentcate user
userSchema.statics.authenticate = function (username, password, callback) {

    User.findOne({ username: username }).exec(function (err, user) {
        if (err) {
            console.log(err);
            return callback(err)
        } 
        else if (!user) {
            err = new Error('User not found.');
            err.status = 401;
            return callback(err);
        }

        bcrypt.compare(password, user.password, function (err, result) {
            if (result === true) return callback(null, user);
            else return callback();
        });
    });
},


userSchema.pre('save', function (next) {

    var user = this;
    try{
        bcrypt.hash(user.password, 10, function (err, hash) {
            if (err) {
                console.log(err);
                return next(err);
            }

            user.password = hash;
            next();
        });
    }
    catch{
        return res.status(500).json({message: "Error in password hashing."});
    }

});

var User = mongoose.model('user', userSchema);
module.exports = User;