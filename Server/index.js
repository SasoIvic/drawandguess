const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const {handleConnection} = require('./game/handleClient.js');

const userRoute = require('./routes/userRoute.js');
const roomRoute = require('./routes/roomRoute.js');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

app.use(cors());
app.use(bodyParser.json());

//Routers
app.use('/user', userRoute);
app.use('/room', roomRoute);

//Database connection
const connectionString = "mongodb+srv://drawGuessUser:drawandguesspassword123@cluster0.hjgdj.mongodb.net/drawandguess?retryWrites=true&w=majority";
mongoose.connect(process.env.MONGODB_URI || connectionString, {
  useNewUrlParser: true,  useUnifiedTopology: true }).then(() => {
	console.log("Successfully connected to the database");
}).catch(err => {
	console.log("Could not connect to the database. Exiting now...", err);
	process.exit();
});
mongoose.Promise = global.Promise;

//Socket connection
io.on('connect', (socket) => {
  console.log("New socket connection.");
  handleConnection(socket, io);
});

//Start server
const PORT = process.env.PORT || 8000;
server.listen(PORT, () => console.log('Server running on port ' + PORT));