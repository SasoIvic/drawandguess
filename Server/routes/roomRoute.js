const express = require('express');
const controller = require('../controllers/roomController.js');
const router = express.Router();

router.get('/:id', controller.getRoom);
router.get('/', controller.getRooms);

router.post('/', controller.addRoom);
router.post('/startGame', controller.startGame);


router.put('/addPlayerInRoom', controller.addPlayerInRoom);
router.put('/removePlayerFromRoom', controller.removePlayerFromRoom);
router.put('/:id', controller.updateRoom);
router.delete('/:id', controller.deleteRoom);

module.exports = router;