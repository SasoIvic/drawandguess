var uuid = require('uuid');
const userModel = require('../models/userModel.js');
const {words} = require('../words.js');

//Array of all active rooms
const rooms = [
    {
        _id: uuid.v4(),
        name: 'Default Room',
        users: [],
        playerDrawing: null,
        currentWord: "",
        hasGameStarted: false,
        enoughPlayersToStart: false,
        timeCountDown: null
    },
];

const getRoom = (roomId) => {

    const room = rooms.find((room) => room._id === roomId);

    if(!room) return { error: 'Room not found.' };
    return room;
}

const addUserInRoom = (user, userId, roomId) => {

    const room = getRoom(roomId);

    if(room.error){
        return { error: room.error }
    }

    if(!room.hasGameStarted) {     
        console.log("Add new user in room.");

        const newUser = {
            _id: userId,
            username: user.username,
            points: 0,
            doneDrawing: false,
        }

        room.users.push(newUser);
        return { error: null, userRoom: room, gameStarted: room.hasGameStarted};
    } 
    else {
        return { error: 'Game has already started. New players can not be added.' }
    }
};

const removeUserFromRoom = (userId, roomId) => {

    const room = getRoom(roomId);

    if(room.hasStarted) return { error: 'Game has started. Cannot remove players' }

    const userIndex = room.users.findIndex((u) => u._id.toString() === userId.toString());

    if(userIndex > -1){
        room.users.splice(userIndex, 1);
        return { error: null };
    } 
    else {
        return { error: "User is not in room" };
    }
}

const startGame = (roomId, userId) => {
    console.log("Starting the game.");

    const room = getRoom(roomId);
    const userIndex = room.users.findIndex((u) => u._id.toString() === userId);

    if(userIndex === -1) 
        return { error: 'User is not in room.' };

    if(room.users.length < 3) 
        return { error: 'Not enough players' };

    room.hasGameStarted = true;

    return { error: null, room: room };
};

const isUserDrawing = (roomId, authData) => {
    
    const room = getRoom(roomId);

    if(!room.playerDrawing) 
        return { error: 'It is noones turn to draw.' };

    if(room.playerDrawing._id.toString() === authData._id.toString())
        return { isDrawing: true };
    else
        return { isDrawing: false };
};



//start game in 3s
const handleGame = (socket, io, roomId) => {
    setTimeout(()=>nextPlayerTurn(io, roomId), 3000);
};

//choose next player drawing and check game over
const nextPlayerTurn = (io, roomId) => {

    const {room} = getRoom(roomId);

    //pick random player
    const nextDrawingCandidate = room.users.filter(u => !u.doneDrawing);

    if(nextDrawingCandidate){

        room.playerDrawing = nextDrawingCandidate[Math.floor(Math.random() * possiblePlayers.length)];
        room.playerDrawing.doneDrawing = true;
        room.currentWord = words[Math.floor(Math.random() * words.length)];

        //tell new word only to player drawing
        io.to(room.playerDrawing.socketId).emit('currentWord', room.currentWord);

        //tell to room who is drawing and start time (3 minutes)
        io.to(roomId).emit('nextTurn', { playerDrawing: room.playerDrawing.username, time: 3*60});

        remainingTime(io, roomId);

    }
    else{
        console.log("Game over.");

        let winner = null;
        let points = 0;

        //find winner
        room.users.forEach((player) => {

            if(player.points > points){

                winner = player;
                points = player.points;
            }
        });

        io.to(roomId).emit('gameOver', winner.username);

        //save points for all players
        room.users.forEach((player) => {

            userModel.findOne({_id: player._id}).exec(function (err, user) {

                if(err) return false;          
                if(!user) return false;

                user.points += player.points;
                user.gamesPlayedNum += 1;     
                if(winner.username === user.username) user.winsNum += 1;

                user.save(function (err, user) {
                    if (err) return false;
                });
            });
        });

        //remove room from array of all rooms
        rooms.splice(rooms.findIndex((r) => r.id.toString() === roomId.toString()), 1);

        return true;
    }

};

//count down 3min timer
const remainingTime = (io, roomId) => {
    
    const {room} = getRoom(roomId);
    room.timeCountDown = 180; //180 s = 3 min

    room.timeCountDown = setInterval(() => {
        room.timeCountDown--;

        io.to(roomId).emit('timeCountdown', { time: getTimeStringFromSeconds(room.counter), users: room.users});

        if(room.timeCountDown <= 0){

            room.currentPlayer = null;
            clearInterval(room.timeCountDown);

            endOfTurn(io, roomId, null, timeCountDown);
        }

    }, 1000);
};

//word guessed correctly or timer was out
const endOfTurn = (io, roomId, winnerOfTurn, timeCountDown) => {

    const {room} = getRoom(roomId);

    if(user !== null)
        clearInterval(room.countDown);

    io.to(roomId).emit('turnFinished');

    let pointsWinner;
    let pointsPlayerDrawing;
    let winner;

    if(timeCountDown > 0){
        pointsWinner = Math.floor(10 * playerTime);
        pointsPlayerDrawing = Math.floor(pointsWinner / 2);

        winner = room.users.find(user => user.username === winnerOfTurn.username);
        const currentPlayer = room.users.find(user => user.username === room.currentPlayer.username);

        if(winner) 
            winner.points += pointsWinner;

        if(currentPlayer)
            currentPlayer.points += pointsPlayerDrawing;
    }

    io.to(roomId).emit('timeCountdown', { time: 0, users: room.users});

    //broadcast winner and word
    io.to(roomId).emit('result', { winner: winner ? user.username : null, word: room.currentWord});

    room.currentPlayer = null;
    room.currentWord = "";

    //nex player starts in 3s
    setTimeout(()=>nextPlayerTurn(io, roomId), 3000);
};

module.exports = {
    rooms,
    getRoom,
    addUserInRoom,
    removeUserFromRoom,
    startGame
} 
