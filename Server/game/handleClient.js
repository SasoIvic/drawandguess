const userModel = require('../models/userModel.js');
const {rooms, getRoom, addUserInRoom} = require('./game.js');
const getTokenData = require('../token.js');

//On new socket connection --> (io.on('connect') v index.js)
const handleConnection = (socket, io) => {

    //join to the room with _id == roomId
    socket.on('join', ({ roomId, userId }, callback) => {
        
        console.log(userId + " just joined.");

        userModel.findOne({_id: userId}).exec(function (err, user) {
            if (err) 
                return res.status(500).json({error: "Error getting user from db."});
            if (!user)
                return res.status(500).json({error: "That user does not exist."});

            let room = getRoom(roomId);
            
            if(room.hasGameStarted) {
                socket.emit('message', { sender: "Admin", message: "Welcome " + user.username + ". Guess what other players are drawing with typing the exact word into the chat."});
                
            }

            socket.join(roomId);
        });
    });

    socket.on('message', ({ username, message, roomId }, callback) => {
        
        io.to(roomId).emit('message', { sender: username, message: message});
    });
}

module.exports = {handleConnection}
