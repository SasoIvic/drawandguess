import React, {useState, useEffect, useRef} from 'react'
import {createUseStyles} from 'react-jss'
import TextField from '@material-ui/core/TextField';
import { FiSend } from 'react-icons/fi';


const useStyles = createUseStyles({

    Chat:{
        height: '90%',
        width: '100%',
        position: 'relative',
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: '10%',


        '& .content':{
            width: '90%',
            height: '90%',
            maxHeight: '700px', 
            overflowY: 'auto',
    
            '&::-webkit-scrollbar': { 
                display: 'none'
            }
        }

    },

    Message:{
        
        marginBottom: '5px',
        width: '100%',

        '& .incoming':{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',


            '& .avatar':{
                marginRight: '10px',
                padding: '12px',
                borderRadius: '15px',
                backgroundColor: '#f1ce8b',
                fontWeight: '800',
                alignSelf: 'baseline'
            }
        },
        '& .outgoing':{
            display: 'flex',
            flexDirection: 'row-reverse',
            alignItems: 'center',

            
            '& .avatar':{
                marginLeft: '10px',
                padding: '12px',
                borderRadius: '15px',
                backgroundColor: '#6495ED',
                fontWeight: '800',
                alignSelf: 'baseline'
            }
        },

        '& .bubble':{
            backgroundColor: 'white',
            border: '1px solid #E8E8E8',
            borderRadius: '12px',
            padding: '15px',
            maxWidth: '65%',
            textAlign: 'left'
        }
    },

    MsgInput:{
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',



        '& .Input':{
            width: '70%',
        },
        '& .Btn':{
            color: '#6495ED',
            fontWeight: 'bold',
            fontSize: '25px',
            marginLeft: '5px'

        }
    },
    
})

const ChatBox = ({socket, messages}) => {
    const classes = useStyles();

    const messagesEnd = useRef(null);   
    const [message, setMessage] = useState("");

    function SendMessage() {
        let path = window.location.pathname;
        var id = path.split('/')[2];

        socket.emit('message', { username: sessionStorage.getItem('username'), message: message, roomId: id});
    }

    const scrollToBottom = () => {
        messagesEnd.current.scrollIntoView({ behavior: "smooth" })
    }

    useEffect(scrollToBottom, [messages]);

    return (
        <div className={classes.Chat}>
            <div className={"content"}>
            {messages && messages.length > 0 && messages.map((msg, index) => (
                <div key={index} className={classes.Message}>
                    <div className={msg.sender != sessionStorage.getItem('username') ? "incoming" : "outgoing"}>
                        <div className={"avatar"}>
                            {msg.sender.substring(0, 2).toUpperCase()}
                        </div>
                        <div className={"bubble"}>
                            {msg.message}
                        </div>
                    </div>
                </div>
            ))}
                <div className={"dummyLastMessage"} ref={messagesEnd}></div>
            </div>

            <div className={classes.MsgInput}>
                <TextField
                    className={"Input"}
                    value={message}
                    onChange={(e)=>setMessage(e.target.value)}
                    variant="outlined"
                    label="Guess the drawing ..."
                />

                <div 
                    className={"Btn"}
                    onClick={SendMessage}
                >
                    <FiSend />
                </div>
            </div>
        </div>
    )
}

export default ChatBox;
