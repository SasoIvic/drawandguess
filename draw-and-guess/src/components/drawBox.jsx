import React, {useState} from 'react'
import {createUseStyles} from 'react-jss'
import Timer from './timer'
import Canvas from './canvas'
import { FaUndoAlt } from 'react-icons/fa';
import { RiDeleteBin5Fill } from 'react-icons/ri';
import { TiArrowBack } from 'react-icons/ti';
import { useHistory } from "react-router-dom";

const useStyles = createUseStyles({

    DrawBox:{
        height: '100%',
        width: '100%',
        position: 'relative',
        alignItems: 'center'
    },
    TimerBox:{
        position: 'relative',
        height: '10%',
        width: '100%',
        borderTopRightRadius: '30px',
        borderTopLeftRadius: '30px',
        top: '0px',
        left: '0px',
        display: 'flex',
        justifyContent: 'center'

    },
    CanvasBox:{
        height: '80%',
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    TaskBox:{
        display: 'flex',
        justifyContent: 'center'
    },
    Task:{
        backgroundColor: '#D6EAF7',
        borderTopRightRadius: '15px',
        borderTopLeftRadius: '15px',
        width: 'fit-content',
        height: 'fit-content',
        padding: '10px 25px 10px 25px',
        color: '#6495ED',
        fontWeight: 'bold',
        fontSize: '20px',
        position: 'absolute',
        bottom: '0px',


    },
    ToolBox:{
        height: '90%',
        paddingLeft: '20px',
        paddingRight: '20px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center'
    },
    ToolBoxButton:{
        height: '50px',
        width: '50px',
        border: '2px solid #6495ED',
        borderRadius: '30px',
        marginTop: '10px',
        marginBottom: '10px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        color: '#6495ED',

        '&:hover':{
            backgroundColor: '#D6EAF7'
        }
    }
})

const tasks = [
    "miza",
    "stol",
    "telefon",
    "prenosni računalnik",
    "avto",
    "vlak",
    "torbica",
    "letalo"
]

const GamePage = props => {
    const classes = useStyles();
    let history = useHistory();

    function goBackToDash() {
        history.push("/");
    }

    const [randomItem, setRandomItem] = useState("");
    const [saveDrawingData, setSaveDrawingData] = useState("");

    const [undo, setUndo] = useState(false);
    const [clear, setClear] = useState(false);

    //Na začetku in ko se zamenja player za risanje
    React.useEffect(() => {
        setRandomItem(tasks[Math.floor(Math.random() * tasks.length)]);
    });

    return (
        <div className={classes.DrawBox}>
            <div className={classes.TimerBox}>
                <div 
                    className={classes.ToolBoxButton}
                    style={{position: 'absolute', left: '10px', border: 'none'}}
                    onClick={goBackToDash}
                >
                    <TiArrowBack style={{height: '70%', width: '70%', marginRight: '3px'}}/>
                </div>
                <Timer />
            </div>

            <div className={classes.CanvasBox}>
                <div className={classes.ToolBox}>
                    <div 
                        className={classes.ToolBoxButton}
                        onClick={()=>setUndo(!undo)}
                    >
                        <FaUndoAlt style={{height: '40%', width: '40%'}}/>
                    </div>
                    <div 
                        className={classes.ToolBoxButton}
                        onClick={()=>setClear(!clear)}
                    >
                        <RiDeleteBin5Fill style={{height: '50%', width: '50%'}}/>
                    </div>
                </div>
                <Canvas saveDrawingData={saveDrawingData} undo={undo} clear={clear}/>
            </div>

            <div className={classes.TaskBox}>
                <div className={classes.Task}>{randomItem && randomItem}</div>
            </div>

        </div>
    )
}

export default GamePage;
