import React from 'react'
import {createUseStyles} from 'react-jss'
import {Card} from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import { useHistory } from "react-router-dom";
import {httpPut} from '../fetcher';

const SERVER_URL = "http://127.0.0.1:8000/";

const useStyles = createUseStyles({

    Card: {
        maxWidth: 345,
        height: 250,
        borderRadius: '15px !important',

        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        border: '2px solid #f1ce8b',

        '& h2':{
            color: isAddRoomCard => isAddRoomCard ? '#f1ce8b' : '#fdfbf5'
        },

        '& p':{
            color: '#fdfbf5',
            marginTop: '0px',
            marginBottom: '20px'
        }
    },

    JoinBtn:{
        width: '20%',
        padding: '8px 12px 8px',
        color: '#fdfbf5',
        fontSize: '18px',
        fontWeight: '750',
        border: '1px solid #fdfbf5',
        borderRadius: '15px',
        cursor: 'pointer',

        '&:hover':{
            backgroundColor: '#f1d78b'
        }
    },

    CreateRoomBtn:{
        width: '20%',
        padding: '8px 12px 8px',
        color: '#F1CE8B',
        fontSize: '18px',
        fontWeight: '750',
        border: '2px solid #F1CE8B',
        borderRadius: '15px',
        cursor: 'pointer',
        marginTop: '10px',

        '&:hover':{
            backgroundColor: '#fefdfa'
        }
    }

})


const RoomCard = ({id, isAddRoomCard, title, playersNum, createNewRoom, roomName, setRoomName}) => {
    const classes = useStyles(isAddRoomCard);
    let history = useHistory();

    function goToWaitingRoom() {

        //add player to room if it is not in the room
        httpPut(SERVER_URL + "room/addPlayerInRoom/", {userId: sessionStorage.getItem('userId'), roomId: id}, data => {
            if(data.message == 'success'){

                history.push("/waiting-room/" + id);
            }
            else{
                console.log(data.message);
            }
        });
    }

    return (
        <Card className={classes.Card} style={{backgroundColor: isAddRoomCard ? "rgba(0,0,0,0)" : "#f1ce8b"}} >
            {isAddRoomCard &&
                <>
                    <h2>Create New Room</h2>

                    <TextField
                        id="outlined-simple-start-adornment"
                        className={classes.RoomNameInput}
                        value={roomName}
                        onChange={(e)=>setRoomName(e.target.value)}
                        variant="outlined"
                        label="Room Name"
                    />

                    <div 
                        className={classes.CreateRoomBtn}
                        onClick={createNewRoom}
                    >
                        Create
                    </div>
                </>
            }

            {!isAddRoomCard &&
                <>
                    <h2>{title}</h2>

                    {playersNum == 8 ? 
                        <p>Room is full</p> :
                        <p>{playersNum} / 8</p>
                    }
        
                    <div 
                        className={classes.JoinBtn}
                        onClick={goToWaitingRoom}
                    >
                        Join
                    </div>
                </>
            }
        </Card>

    )
}

export default RoomCard;
