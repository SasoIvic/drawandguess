import React, {useEffect, useRef} from "react";
import CanvasDraw from "react-canvas-draw";
import {createUseStyles} from 'react-jss'


const useStyles = createUseStyles({

    Canvas:{
        width: '80%',
        height: '90%'
    }
    
})

const Canvas = ({saveDrawingData, undo, clear}) => {
    const classes = useStyles();
    const canvas = useRef(null)

    useEffect(() => {
        canvas.current?.clear();
    }, [clear])

    useEffect(() => {
        canvas.current?.undo();
    }, [undo])

    return (
    <div className={classes.Canvas}>    
        <CanvasDraw 
            ref={canvas}
            hideGrid={true}
            style={{ width: '100%', height: '100%' }}
            saveData={saveDrawingData}   // potem dobimo rezultat z getSaveData() in pošljemo preko socket.io
            brushRadius={5}
            lazyRadius={1}
        />
    </div>
    );
}

export default Canvas;
