import React from 'react'
import {createUseStyles} from 'react-jss'

const useStyles = createUseStyles({

    Timer:{
        backgroundColor: '#D6EAF7',
        borderBottomRightRadius: '15px',
        borderBottomLeftRadius: '15px',
        width: 'fit-content',
        height: 'fit-content',
        padding: '10px 25px 10px 25px',
        color: '#6495ED',
        fontWeight: 'bold',
        fontSize: '20px'
    },
})

const Timer = props => {
    const classes = useStyles();

    const [seconds, setSeconds] = React.useState(60);
  
    React.useEffect(() => {
      if (seconds > 0)
        setTimeout(() => setSeconds(seconds - 1), 1000);
      else
        setSeconds('Game Over');
    });
  
    return (
      <div className={classes.Timer}>
          {seconds}
      </div>
    );
  }

  export default Timer;
