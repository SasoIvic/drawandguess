import './App.css';
import DashboardPage from './pages/dashboardPage'
import WaitingRoomPage from './pages/waitingRoomPage'
import GamePage from './pages/gamePage';
import LoginPage from './pages/loginPage';
import RegistrationPage from './pages/registerPage';
import ProfilePage from './pages/profilePage';

import { Route, Switch } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={DashboardPage} />
        <Route exit path="/login" component={LoginPage} />
        <Route exit path="/registration" component={RegistrationPage} />
        <Route path="/waiting-room/" component={WaitingRoomPage} />
        <Route path="/game/" component={GamePage} />
        <Route path="/profile/" component={ProfilePage} />

      </Switch>
    </div>
  );
}

export default App;
