import React, {useEffect, useState} from 'react'
import {createUseStyles} from 'react-jss'
import DrawBox from '../components/drawBox'
import ChatBox from '../components/chatBox'

import io from "socket.io-client"

const SERVER_URL = "http://127.0.0.1:8000/"

const useStyles = createUseStyles({
    GameWrapper:{
        width: '100%',
        height: '100%',
        display: 'grid',
        gridTemplateColumns: '3fr 1fr',
        gridGap: '20px',
        backgroundColor: '#6495ED',
    },
    DrawBox:{
        margin: '20px 0px 20px 20px',
        backgroundColor: '#EBF5FB',
        borderRadius: '30px',
    },
    ChatBox:{
        margin: '20px 20px 20px 0px',
        backgroundColor: '#EBF5FB',
        borderRadius: '30px'
    }
})

var socket;

const GamePage = props => {
    const classes = useStyles();

    const [messages, setMessages] = useState([]);

    useEffect(() => {
        
        let path = window.location.pathname;
        var id = path.split('/')[2];

        socket = io(SERVER_URL);

        socket.emit('join', { roomId: id, userId: sessionStorage.getItem('userId') }, (data) => {
            if(data.error != null) {
                alert(data.error);
            }
        });
 
        socket.on('message', message => {
            setMessages(messages => [ ...messages, message ]);

            console.log(messages);
        });

        return () => socket.disconnect();

    }, [])

    return (
        <div className={classes.GameWrapper}>
            <div className={classes.DrawBox}>
                <DrawBox />
            </div>
            <div className={classes.ChatBox}>
                <ChatBox socket={socket} messages={messages}/>
            </div>
        </div>
    )
}

export default GamePage;
