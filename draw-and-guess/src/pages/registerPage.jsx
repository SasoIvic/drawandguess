import React, {useState, useEffect} from 'react'
import {createUseStyles} from 'react-jss'
import { useHistory } from "react-router-dom";
import TextField from '@material-ui/core/TextField';
import {httpPost} from '../fetcher';

const SERVER_URL = "http://127.0.0.1:8000/";

const useStyles = createUseStyles({
    WaitingRoom:{
        width: '100%',
        height: '100%',
        backgroundColor: '#6495ED',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'cursive',

    },
    Container:{
        backgroundColor: '#EBF5FB',
        borderRadius: '30px',
        width: '40%',
        height: '80%',
        margin: '50px',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        

        '& h2':{
            color: '#6495ED',
            textTransform: 'uppercase',
            fontSize: '6vh',
            marginBottom: '20px',
            textShadow: '1.25px 1.25px #12449f',
        },
        '& h4':{
            color: '#f1d78b',
            fontSize: '3vh',
            textShadow: '1.25px 1.25px #908153',
        },
        '& p':{
            margin: '5px',
            fontSize: '3vh',
            color: '#6495ED',
        },
    },

    Input:{
        marginTop: '30px !important',
    },
    
    RegistrationBtn:{
        width: 'auto',
        padding: '8px 60px 8px',
        color: '#fdfbf5',
        fontSize: '3vh',
        fontWeight: '750',
        border: '1px solid #fdfbf5',
        borderRadius: '15px',
        cursor: 'pointer',
        backgroundColor: '#f1d78b',
        marginTop: '50px',
        marginBottom: '0px',
        textShadow: '1.25px 1.25px #908153',

        '&:hover':{
            backgroundColor: '#F3DFA2'
        },
        
    },
    LoginBtn:{
        width: 'auto',
        padding: '8px 60px 8px',
        color: '#6495ED',
        textShadow: '1px 1px #12449f',
        fontSize: '2vh',
        fontWeight: '750',
        cursor: 'pointer',
        marginTop: '10px',
        marginBottom: '50px',

        '&:hover':{
            color: '#739fee'
        },
        
    },
})


const Login = props => {
    const classes = useStyles();
    let history = useHistory();

    const [email, setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");


    function goToLogin() {
        history.push("/login");
    }

    function register(){
        
        if(email, username, password){
            httpPost(SERVER_URL + "user/register", {email: email, username: username, password:password}, data => {
                if(data.message == 'success'){

                    sessionStorage.setItem('token', data.token);
                    sessionStorage.setItem('userId', data.user?._id);
                    sessionStorage.setItem('username', data.user?.username);
                    history.push('/');
                }
                else{
                    console.log(data.message);
                }
            });
        }
        else{
            console.log("Fill all inputs!");
        }

    }

    return (
        <div className={classes.WaitingRoom}>
            <div className={classes.Container}>
                
                <h2>Draw & Guess</h2>

                <TextField
                    className={classes.Input}
                    value={email}
                    onChange={(e)=>setEmail(e.target.value)}
                    variant="outlined"
                    label="Email"
                />

                <TextField
                    className={classes.Input}
                    value={username}
                    onChange={(e)=>setUsername(e.target.value)}
                    variant="outlined"
                    label="Username"
                />

                <TextField
                    type={"password"}
                    className={classes.Input}
                    value={password}
                    onChange={(e)=>setPassword(e.target.value)}
                    variant="outlined"
                    label="Password"
                />

                <div 
                    className={classes.RegistrationBtn}
                    onClick={register}
                >
                    Registration
                </div>
                
                <div 
                    className={classes.LoginBtn}
                    onClick={goToLogin}
                >
                    Login
                </div>

            </div>
        </div>
    )
}

export default Login;
