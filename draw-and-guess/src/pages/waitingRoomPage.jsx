import React, {useState, useEffect} from 'react';
import {createUseStyles} from 'react-jss';
import { useHistory } from "react-router-dom";
import {httpGet, httpPost, httpPut} from '../fetcher'

const SERVER_URL = "http://127.0.0.1:8000/";
var roomId = "";

const useStyles = createUseStyles({
    WaitingRoom:{
        width: '100%',
        height: '100%',
        backgroundColor: '#6495ED',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'cursive',

    },
    Container:{
        backgroundColor: '#EBF5FB',
        borderRadius: '30px',
        width: '40%',
        height: '80%',
        margin: '50px',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        

        '& h2':{
            color: '#6495ED',
            textTransform: 'uppercase',
            fontSize: '8vh',
            marginBottom: '20px',
            textShadow: '1.25px 1.25px #12449f',
        },
        '& h4':{
            color: '#f1d78b',
            fontSize: '3vh',
            textShadow: '1.25px 1.25px #908153',
        },
        '& p':{
            margin: '5px',
            fontSize: '3vh',
            color: '#6495ED',
        },
    },
    
    StartGameBtn:{
        width: 'auto',
        padding: '8px 20px 8px',
        color: '#fdfbf5',
        fontSize: '4vh',
        fontWeight: '750',
        border: '1px solid #fdfbf5',
        borderRadius: '15px',
        cursor: 'pointer',
        backgroundColor: '#f1d78b',
        marginTop: '50px',
        marginBottom: '50px',
        textShadow: '1.25px 1.25px #908153',

        '&:hover':{
            backgroundColor: '#F3DFA2'
        },
        
    },
})

const WaitingRoom = props => {
    const classes = useStyles();
    let history = useHistory();

    const [room, setRoom] = useState(null);

    function goToGame() {

        let path = window.location.pathname;
        var id = path.split('/')[2];

        if(room && room.users.length >= 3){

            //Start game
            httpPost(SERVER_URL + "room/startGame/", {roomId: id, userId: sessionStorage.getItem('userId')}, data => {
                if(data.message == 'success'){

                    httpGet(SERVER_URL + `room/` + id, data => {
                        setRoom(data.room);
                    });

                    history.push("/game/" + id);
                }
                else{
                    console.log(data.message);
                }
            });
        }
    }

    useEffect(() => {
        //tu se pridobi soba glede na id iz URL-ja
        let path = window.location.pathname;
        var id = path.split('/')[2];

        httpGet(SERVER_URL + `room/` + id, data => {
            setRoom(data.room);
            console.log(data);
        });

        //get room data every 2s
        const interval = setInterval(function(){
            let id = path.split('/')[2];
            
            if(id && id != ""){    
                httpGet(SERVER_URL + `room/` + id, data => {
                    setRoom(data.room);

                    //Game started
                    if(data.room && data.room.hasGameStarted){
                        console.log("game started");
                        history.push("/game/" + id);
                    }
                });
            }

        }, 2000);

        return history.listen((location) => {
            console.log(`You changed the page to: ${location.pathname}`)

            //odstrani playerja iz sobe razen če gre na igro
            if(location.pathname != ("/game/" + id)){

                console.log("Remove player from room. RoomId: " + id);

                httpPut(SERVER_URL + "room/removePlayerFromRoom/", {userId: sessionStorage.getItem('userId'), roomId: id}, data => {
                    if(data.message == 'success'){
    
                        httpGet(SERVER_URL + `room/` + id, data => {
                            setRoom(data.room);
                            console.log(data);
                        });
                    }
                    else{
                        console.log(data.message);
                    }
                });
            }

            clearInterval(interval);
        }) 

    }, [])

    return (
        <div className={classes.WaitingRoom}>
            <div className={classes.Container}>
                
                <h2>{room && room.name}</h2>

                <h4>Players waiting: </h4>
                {room && room.users && room.users.map((player, index) => (
                    <p key={player._id}>{player.username}</p>
                ))}

                <div 
                    className={classes.StartGameBtn}
                    onClick={goToGame}
                >
                    Start game
                </div>

            </div>
        </div>
    )
}

export default WaitingRoom;
