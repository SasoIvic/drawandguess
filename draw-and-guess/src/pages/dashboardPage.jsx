import React, {useState, useEffect} from 'react'
import {createUseStyles} from 'react-jss'
import RoomCard from '../components/roomCard'
import { BiLogOut, BiTrophy } from 'react-icons/bi';
import { useHistory } from "react-router-dom";
import {httpGet, httpPost} from '../fetcher'

const SERVER_URL = "http://127.0.0.1:8000/"

const useStyles = createUseStyles({
    Dashboard:{
        width: '100%',
        height: '100%',
        backgroundColor: '#6495ED',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'cursive',
        position: 'relative',
    },
    Titlewrapper:{
        position: 'absolute',
        backgroundColor: '#6495ED',
        borderBottomLeftRadius: '20px',
        borderBottomRightRadius: '20px',
        top: '0',
        width: '30%',
        height: '10%',
        color: 'white',
        fontSize: '4vh',

        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    TopButtons:{
        position: 'absolute',
        backgroundColor: '#f1ce8b',
        borderBottomLeftRadius: '20px',
        borderBottomRightRadius: '20px',
        top: '0px',
        width: '33%',
        height: '8%',
        color: 'white',
        fontSize: '3vh',
        paddingLeft: '10px',
        paddingRight: '10px',

        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',

        '&:hover':{
            width: '35%',
            paddingLeft: '15px',
            paddingRight: '15px',
        }
    },
    ProfileBtn:{
        backgroundColor: 'gray',
        width: '50px',
        height: '50px',
    },
    GameRoomsBG:{
        backgroundColor: '#EBF5FB',
        borderRadius: '30px',
        width: '100%',
        height: '90%',
        margin: '50px',
        display: 'flex',
        justifyContent: 'center',
    },
    GameRoomsWrapper:{
        margin: '50px',
        marginTop: '80px',

        display: 'grid',
        gridTemplateColumns: '1fr 1fr 1fr 1fr 1fr',
        gridGap: '40px',
        columnGap: '40px'
    },    

    '@media screen and (max-width: 1600px)': {
        GameRoomsWrapper:{
            margin: '50px',
            marginTop: '80px',

            display: 'grid',
            gridTemplateColumns: '1fr 1fr 1fr 1fr',
            gridGap: '40px',
            columnGap: '40px'
        },
    },

    '@media screen and (max-width: 1300px)': {
        GameRoomsWrapper:{
            margin: '45px',
            marginTop: '80px',
    
            display: 'grid',
            gridTemplateColumns: '1fr 1fr 1fr',
            gridGap: '30px',
            columnGap: '30px'
        },

        '@media screen and (max-width: 950px)': {
            GameRoomsWrapper:{
                margin: '40px',
                marginTop: '80px',
        
                display: 'grid',
                gridTemplateColumns: '1fr 1fr',
                gridGap: '25px',
                columnGap: '25px'
            },
        },

        '@media screen and (max-width: 650px)': {
            GameRoomsWrapper:{
                margin: '45px',
                marginTop: '80px',
        
                display: 'grid',
                gridTemplateColumns: '1fr',
            },
        },
    },
})

const Dashboard = props => {
    const classes = useStyles();
    let history = useHistory();

    const [rooms, setRooms] = useState([]);
    const [roomName, setRoomName] = useState(" ");

    useEffect(() => {
        console.log("Fetch rooms: ");

        httpGet(SERVER_URL + `room/`, data => {
            setRooms(data.rooms);
            console.log(data);
        });

        //get rooms every 10s
        const interval = setInterval(function(){     
            httpGet(SERVER_URL + `room/`, data => {
                setRooms(data.rooms);
            });
        }, 10000);


        return () => {
            clearInterval(interval);
        }

    }, [])



    const Loguot = () => {
        alert("Logout");
    }

    const GoToProfile = () => {
        history.push("/profile");
    }

    const createNewRoom = () => {
        if(roomName && roomName != " "){

            httpPost(SERVER_URL + "room/", {name: roomName, userId: sessionStorage.getItem('userId')}, data => {
                if(data.message == 'success'){

                    httpGet(SERVER_URL + `room/`, rooms => {
                        setRooms(rooms.rooms);
                    });

                    history.push('/waiting-room/' + data.room._id);
                }
                else{
                    console.log(data.message);
                }
            });
        }
        else{
            console.log("Fill new room name!");
        }
    }

    return (
        <div className={classes.Dashboard}>
            <div className={classes.TopButtons}>
                <div onClick={GoToProfile}><BiTrophy/></div>
                <div onClick={Loguot}><BiLogOut/></div>
            </div>
            <div className={classes.Titlewrapper}>
                <p>Draw & Guess</p>
            </div>

            <div className={classes.GameRoomsBG}>
                <div style={{width: '100%'}}>
                    <div className={classes.GameRoomsWrapper}>
                        <RoomCard 
                            key={-1}
                            id={-1} 
                            isAddRoomCard={true} 
                            title={null} 
                            playersNum={null} 
                            createNewRoom={createNewRoom}
                            roomName={roomName} 
                            setRoomName={setRoomName}
                        />


                        {rooms && rooms.map((room, index) => (
                            <RoomCard 
                                key={index} 
                                id={room._id}
                                isAddRoomCard={false} 
                                title={room.name} 
                                playersNum={room.users.length} 
                                createNewRoom={createNewRoom}
                                roomName={room.name} 
                                setRoomName={setRoomName}
                            />
                        ))}

                    </div>
                </div>
            </div>
        </div>
    )
}

export default Dashboard;
