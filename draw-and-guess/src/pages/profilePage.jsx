import React, {useState, useEffect} from 'react'
import {createUseStyles} from 'react-jss'
import { useHistory } from "react-router-dom";
import { TiArrowBack } from 'react-icons/ti';
import { BiTrophy } from 'react-icons/bi';

const useStyles = createUseStyles({
    WaitingRoom:{
        width: '100%',
        height: '100%',
        backgroundColor: '#6495ED',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'cursive',

    },
    Container:{
        backgroundColor: '#EBF5FB',
        borderRadius: '30px',
        width: '30%',
        height: '80%',
        margin: '50px',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        position: 'relative',
        
        '& h2':{
            color: '#6495ED',
            textTransform: 'uppercase',
            fontSize: '8vh',
            marginBottom: '20px',
            marginTop: '20px',
            textShadow: '1.25px 1.25px #12449f',
        },
        '& h4':{
            color: '#f1d78b',
            fontSize: '3vh',
            textShadow: '1.25px 1.25px #908153',
        },
        '& p':{
            margin: '5px',
            fontSize: '3vh',
            color: '#6495ED',
        },
    },
    
    ToolBoxButton:{
        position: 'absolute',
        top: '10px',
        left: '15px',
        height: '50px',
        width: '50px',
        border: '2px solid #6495ED',
        borderRadius: '30px',
        marginTop: '10px',
        marginBottom: '10px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        color: '#6495ED',

        '&:hover':{
            backgroundColor: '#D6EAF7'
        }
    },

    Row:{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        color: 	'#6495ED',
        fontSize: '2vh'
    }
})

const Profile = props => {
    const classes = useStyles();
    let history = useHistory();

    function goBackToDash() {
        history.push("/");
    }

    useEffect(() => {
        //tu se pridobi soba glede na id iz URL-ja
        let path = window.location.pathname;
        let id = path.split('/')[2];

    })

    return (
        <div className={classes.WaitingRoom}>
            <div className={classes.Container}>

                <div 
                    className={classes.ToolBoxButton}
                    onClick={goBackToDash}
                >
                    <TiArrowBack style={{height: '70%', width: '70%', marginRight: '3px'}}/>
                </div>
                
                <BiTrophy style={{color: '#F1CE8B', height: '80px', width: '80px'}}/>

                <h2>SASO</h2>

                <div className={classes.Row}>
                    <>Number of games played: </>
                    <>16</>
                </div>

                <div className={classes.Row}>
                    <>Number of wins: </>
                    <>10</>
                </div>
                
                <div className={classes.Row}>
                    <>Total points: </>
                    <>1050</>
                </div>

            </div>
        </div>
    )
}

export default Profile;
